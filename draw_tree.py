"""Draw a tree given the height of the tree."""
from itertools import count


def draw_tree():
    """Draw tree from asteriks."""
    height = int(input('Enter height of tree: '))
    width = 2 * height - 1
    spaces_b4_asteriks = count(start=width//2, step=-1)

    for i in range(1, height+1):
        no_of_asteriks = 2 * i - 1
        asteriks = '*' * no_of_asteriks

        spaces = ' ' * next(spaces_b4_asteriks)
        print(spaces, asteriks, sep='')
