"""Print prime numbers upto a certain number."""

def print_prime_numbers():
    """Compute and print out prime numbers not greater than the given value."""
    max_prime = int(input('Enter upper bound for the prime numbers: '))
    for i in range(2, max_prime+1):
        if any(i%j == 0 for j in range(2, i)):
            continue
        print(i, end=', ')
